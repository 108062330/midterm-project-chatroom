# Software Studio 2021 Spring Midterm Project
## Notice
* 108062330 莊景堯

## Topic
* Project Name : Midterm Project - Chatroom


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|auto scroll down|X|Y|
|multiple chatroom notify|X|Y|
|enter enable|X|Y|
|user recognize|X|Y|
|scroll to bottom button|X|Y|
|cute cat background image|X|Y|

# 作品網址：https://yccchatroom.web.app

## Website Detail Description
Chatroom，在主要概念上盡量讓他可以與目前主流的通訊軟體有人性化的體驗，在使用上不會有不順眼等等問題，主要功能如下：


# Components Description : 
1. Sign In, Sign Up: 
    * 連入index.html後如果尚未登入，會自動重新導向至signIn.html。
    * 在Sign Up, Sign In進行操作後皆會有回傳結果的訊息框（重複使用者名稱, email, 密碼錯誤, etc.）
2. public chatroom:
    * 上方標題顯示目前聊天室（public為共用），並且顯示目前的聊天計數器，右邊有邀請他人的按鈕。
    * 邀請他人的input與聊天室的input可共用enter鍵傳送，取決於邀請他人的懸浮div是否顯示。
    * 發送email進行邀請後，會有三種其中之一的結果（成功、已在聊天室中、email無人註冊）
3. 聊天主視窗:
    * 輸入文字後自動將視窗往下滾動。
    * 當回顧歷史訊息時會出現置底按鈕。
    * 當接收訊息時自動往下滾動，但如果正在回顧歷史訊息則不會滾動。
    * 訊息分側。
    * 訊息框內嵌收回訊息按鈕（馬上新增的訊息可以，之前的history有bugQQQ）。
4. 左側聊天室選單
    * 下方可新增聊天室。
5. 同時可以接收多個聊天室的通知


6. RWD:
    * 主要使用flex進行自動調整大小，如果使用手機或者width較小的螢幕，左側選單會從占用25%變成占用100%。
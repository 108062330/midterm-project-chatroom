$(document).ready(() => {
    firebase.auth().onAuthStateChanged(() => {
        user = firebase.auth().currentUser;
    });

    let database = firebase.database();

    $("body").keyup(evt => {
        if (evt.which === 13) {
            if (!$("#headingOne button").hasClass("collapsed"))
                $("#SignInBtn").click();
            else if (!$("#headingTwo button").hasClass("collapsed"))
                $("#SignUpBtn").click();
        }
    });

    $("#SignInBtn").click(() => {
        let email = $("#SignInInputEmail").val();
        let password = $("#SignInInputPassword").val();

        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then(
                () => {
                    $("#SignInForm").before(
                        '<div class="alert_SignIn alert alert-success" role="alert">Sign in successfully! Redirecting...</div>'
                    );

                    setTimeout(() => {
                        window.location.href = "../index.html";
                    }, 1000);
                },
                err => {
                    $("#SignInForm").before(
                        '<div class="alert_SignIn alert alert-danger" role="alert">' +
                        err.message +
                        "</div>"
                    );

                    setTimeout(() => {
                        $(".alert_SignIn").fadeOut(2000);
                    }, 3000);
                }
            );
    });

    $("#SignUpBtn").click(() => {
        let user = $("#SignUpInputUsername").val();
        let email = $("#SignUpInputEmail").val();
        let password = $("#SignUpInputPassword").val();

        firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(
                result => {
                    database
                        .ref("user")
                        .child(result.user.uid)
                        .set({ email: email, user: user })
                        .then(() => {
                            database
                                .ref("chatroom/chat/public/user")
                                .child(result.user.uid)
                                .set(0) // TODO: read information.
								.then(() => {
									$("#SignUpForm").before(
										'<div class="alert_SignIn alert alert-success" role="alert">Sign up successfully! Redirecting...</div>'
									);
		
									setTimeout(() => {
										window.location.href = "index.html";
									}, 1000);
								});
                        });
                        
                },
                err => {
                    $("#SignUpForm").before(
                        '<div class="alert_SignUp alert alert-danger" role="alert">' +
                        err.message +
                        "</div>"
                    );

                    setTimeout(() => {
                        $(".alert_SignUp").fadeOut(2000);
                    }, 3000);
                }
            );
    });

    $("#GoogleSignInBtn").click(() => {
        let provider = new firebase.auth.GoogleAuthProvider();

        firebase
            .auth()
            .signInWithPopup(provider)
            .then(result => {
                database
                    .ref("user")
                    .child(result.user.uid)
                    .set({
                        email: result.user.email,
                        user: result.user.displayName,
                    })
                    .then(() => {
                        database
                            .ref("chatroom/chat/public/user")
                            .child(result.user.uid)
                            .set(0); // TODO: read information.
                    })
                    .then(() => {
                        window.location.href = "index.html";
                    });
            });
    });
});
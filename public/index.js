var user; // user raw data.
var Username; // user name.
var current_chatroom = "public";
var database = firebase.database();
var chatListF, chatListS;
var countF, countS;
var init = false;

$(document).ready(() => {
	current_chatroom = "public";
	(chatListF = 0), (chatListS = 0);
	window.addEventListener("resize", () => {
		database
			.ref("chatroom/chat/" + current_chatroom + "/content")
			.once("value", src => {
				for (let i in src.val())
					$("#unsend-" + i).css({
						height: $("#message-" + i).css("height"),
					});
			});
	});

	Notification.requestPermission();

	firebase.auth().onAuthStateChanged(() => {
		user = firebase.auth().currentUser;

		if (!user) {
			window.location.href = "SignIn.html";
		}

		database.ref("user/" + user.uid + "/user").once("value", src => {
			Username = src.val();

			let prefix = ["magician", "witch", "imp", "fairy"];

			$("#title").text(
				"The " + prefix[getRandomInt(4)] + ", " + Username + "."
			);

			initChat();
		});
	});
});

function initChat() {
	let pf = new Promise((resolve, reject) => {
		changeChatroom("public");

		database.ref("chatroom/chatList/public/num").once("value", data => {
			countS = 0 - data.val();
		});

		resolve();
	});

	pf.then(() => {
		$("#logout").click(() => {
			firebase
				.auth()
				.signOut()
				.then(function () {
					alert("User sign out success!");
					window.location.href = "SignIn.html";
				})
				.catch(function (error) {
					alert("User sign out failed!");
				});
		});

		$("#new_chatroom_btn").click(() => {
			let Nchatroom = document.getElementById("new_chatroom_input");

			if (Nchatroom.value.trim() != "") {
				P1 = new Promise((resolve, reject) => {
					database
						.ref("chatroom/chatList")
						.once("value", snapshot => {
							if (snapshot.hasChild(Nchatroom.value)) {
								// TODO: alert room name exist.
								$("#notice_new_chatroom").append(
									'<div class="build_notification alert alert-warning" role="alert">This magic town already exists!</div>'
								);

								setTimeout(() => {
									$(".build_notification").fadeOut(5000);
								}, 3000);

								Nchatroom.value = "";
								reject("exist");
							} else {
								resolve();
							}
						});
				});
				// .then(() => {
				// 	console.log(
				// 		"chatroom/chat/" +
				// 			Nchatroom.value +
				// 			"/user/"
				// 	);
				//
				P1.then(() => {
					database
						.ref("chatroom/chatList/" + Nchatroom.value + "/num")
						.set(0);
					database
						.ref("chatroom/chat/" + Nchatroom.value + "/user/")
						.child(user.uid)
						.set(0)
						.then(
							() => {
								$("#notice_new_chatroom").append(
									'<div class="build_notification alert alert-success" role="alert">Woo hoo! Now you are the mayor of this new town!</div>'
								);

								database
									.ref(
										"chatroom/chat/" +
											Nchatroom.value +
											"/content"
									)
									.on("child_added", src => {
										database
											.ref(
												"chatroom/chat/" +
													current_chatroom +
													"/content"
											)
											.once("value", res => {
												if (res.hasChild(src.key)) {
													countS++;

													if (countS > 0) {
														$(
															"#chatContent"
														).append(
															chatHText(
																src.val(),
																src.key
															)
														);

														$(
															"#unsend-" + src.key
														).css({
															height: $(
																"#message-" +
																	src.key
															).css("height"),
														});
														$(
															"#unsend-" + src.key
														).click(evt => {
															$(
																"#" +
																	evt.target
																		.id
															).after(
																'<div class="float-right fade show alert_unsend_message alert alert-danger" role="alert" style="margin:0px 10px; width: 430px; position: relative; bottom: 20px;">' +
																	"Do you want to delete this message?" +
																	"<div style='display: inline-block; margin: 0px 10px; padding: 5px 20px; color: darkred; border-radius: 3px; border: 1px darkred solid;' id='yes-" +
																	evt.target
																		.id +
																	"'>Yes</div>" +
																	"<div style='display: inline-block; margin: 0px 5px; padding: 5px 15px;  color: green; border-radius: 3px; border: 1px green solid;' id='no-" +
																	evt.target
																		.id +
																	"'>No</div>" +
																	"</div>"
															);

															$(
																"#yes-" +
																	evt.target
																		.id
															).click(() => {
																console.log(
																	"hello"
																);
																database
																	.ref(
																		"chatroom/chat/" +
																			current_chatroom +
																			"/content/" +
																			evt.target.id.slice(
																				7
																			) +
																			"/content"
																	)
																	.set(
																		"Unsend a message"
																	);
																$(
																	"#message-" +
																		evt.target.id.slice(
																			7
																		)
																).text(
																	"Unsend a message"
																);

																setTimeout(
																	() => {
																		$(
																			".alert_unsend_message"
																		).fadeOut(
																			500
																		);
																	},
																	500
																);
															});
															$(
																"#yes-" +
																	evt.target
																		.id
															).hover(
																evt => {
																	evt.target.style[
																		"color"
																	] = "white";
																	evt.target.style[
																		"background-color"
																	] = "red";
																	evt.target.style[
																		"border"
																	] =
																		"1px transparent solid";
																},
																evt => {
																	evt.target.style[
																		"color"
																	] =
																		"darkred";
																	evt.target.style[
																		"background-color"
																	] =
																		"transparent";
																	evt.target.style[
																		"border"
																	] =
																		"1px darkred solid";
																}
															);

															$(
																"#no-" +
																	evt.target
																		.id
															).hover(
																evt => {
																	evt.target.style[
																		"color"
																	] = "white";
																	evt.target.style[
																		"background-color"
																	] = "green";
																	evt.target.style[
																		"border"
																	] =
																		"1px transparent solid";
																},
																evt => {
																	evt.target.style[
																		"color"
																	] = "green";
																	evt.target.style[
																		"background-color"
																	] =
																		"transparent";
																	evt.target.style[
																		"border"
																	] =
																		"1px black solid";
																}
															);

															$(
																"#no-" +
																	evt.target
																		.id
															).click(() => {
																setTimeout(
																	() => {
																		$(
																			".alert_unsend_message"
																		).fadeOut(
																			500
																		);
																	},
																	500
																);
															});

															setTimeout(() => {
																$(
																	".alert_unsend_message"
																).fadeOut(5000);
															}, 3000);
														});
														// ----------------------------------------------------
														let chatBox = document.getElementById(
															"chatContent_div"
														);
														let diff =
															chatBox.scrollHeight -
															chatBox.clientHeight -
															chatBox.scrollTop;
														if (diff < 700)
															scrollToBottom();

														if (
															src.val().uid !=
															user.uid
														) {
															send_notification(
																current_chatroom,
																src.val().user,
																src.val()
																	.content
															);
														} else scrollToBottom();
													}
												}
											});
									});

								setTimeout(() => {
									$(".build_notification").fadeOut(5000);
								}, 3000);

								Nchatroom.value = "";
								// TODO: alert successful.
							},
							() => {}
						);
				});
				/*
				err => {
					//TODO: invalid.
					$("#notice_new_chatroom").append(
						'<div class="build_notification alert alert-danger" role="alert">Error! ' +
							err.message +
							"</div>"
					);

					setTimeout(() => {
						$(".build_notification").fadeOut(5000);
					}, 3000);

					Nchatroom.value = "";
                    */
			}
		});

		$("#sendBtn").click(() => {
			let message = document.getElementById("message-to-send");

			let now = new Date();

			if (message.value.trim() != "") {
				database
					.ref("chatroom/chat/" + current_chatroom + "/content")
					.push({
						content: message.value,
						date: now.getMonth() + "/" + now.getDay(),
						time:
							String(now.getHours()).padStart(2, "0") +
							":" +
							String(now.getMinutes()).padStart(2, "0"),
						type: 0,
						uid: user.uid,
						url: null,
						user: Username,
					});

				scrollToBottom();
			}

			message.value = "";
		});

		$("#add_member_img").click(() => {
			let input_email = $("#add_member_input").val();
			let available_uid = "none";
			let available_username;
			database.ref("user").once("value", data => {
				for (let i in data.val()) {
					if (data.val()[i].email === input_email) {
						available_uid = i;
						available_username = data.val()[i].user;
					}
				}

				let current_message_num;
				database
					.ref("chatroom/chatList/" + current_chatroom + "/num")
					.once("value", data => {
						current_message_num = data.val();

						let Ref = database.ref(
							"chatroom/chat/" + current_chatroom + "/user"
						);

						RefData = Ref.once("value", res => {
							database.ref("user").once("value", user_list => {
								if (user_list.hasChild(available_uid)) {
									if (res.hasChild(available_uid)) {
										$("#add_member_img").after(
											'<div class="alert_add_member alert alert-info" role="alert" style="margin:10px">' +
												available_username +
												" is already in this chatroom!" +
												"</div>"
										);

										setTimeout(() => {
											$(".alert_add_member").fadeOut(
												2000
											);
										}, 2000);
									} else
										Ref.child(available_uid)
											.set({
												read: current_message_num,
											})
											.then(() => {
												$("#add_member_img").after(
													'<div class="alert_add_member alert alert-success" role="alert" style="margin:10px">Invite ' +
														available_username +
														" successfully!</div>"
												);

												setTimeout(() => {
													$(
														".alert_add_member"
													).fadeOut(2000);
												}, 2000);
											});
								} else {
									$("#add_member_img").after(
										'<div class="alert_add_member alert alert-danger" role="alert" style="margin:10px">' +
											"User doesn't exist! Please try again." +
											"</div>"
									);

									setTimeout(() => {
										$(".alert_add_member").fadeOut(2000);
									}, 2000);
								}
							});
						});
					});
			});

			document.getElementById("add_member_input").value = "";
		});

		$("body").keyup(evt => {
			if (evt.which === 13) {
				if ($("#add_member_relative").css("display") === "flex")
					$("#add_member_img").click();
				// else if
				else $("#sendBtn").click();
			}
		});

		$("#chatContent_div").scroll(() => {
			let chatBox = document.getElementById("chatContent_div");
			let diff =
				chatBox.scrollHeight - chatBox.clientHeight - chatBox.scrollTop;

			if (diff > 800 && $("#scroll_down_btn").css("display") != "block") {
				$("#scroll_down_btn").css({ display: "block" });
				$("#scroll_down_btn").animate({ bottom: "120px" }, 200);
			} else if (
				diff < 100 &&
				$("#scroll_down_btn").css("display") != "none"
			)
				$("#scroll_down_btn").animate({ bottom: "40px" }, 200, () => {
					$("#scroll_down_btn").css({ display: "none" });
				});
		});

		$("#scroll_down_btn").click(() => {
			scrollToBottom();
		});

		database.ref("chatroom/chatList").once("value", src => {
			for (let i in src.val()) {
				let ref = database.ref("chatroom/chat/" + i + "/user");
				ref.once("value", src => {
					if (src.hasChild(user.uid)) {
						database
							.ref("chatroom/chat/" + i + "/content")
							.on("child_added", src => {
								database
									.ref(
										"chatroom/chat/" +
											current_chatroom +
											"/content"
									)
									.once("value", data => {
										if (data.hasChild(src.key)) {
											database
												.ref(
													"chatroom/chat/" +
														current_chatroom +
														"/content"
												)
												.once("value", data => {
													chatroom_count(
														data,
														current_chatroom
													);
												});

											countS++;

											if (countS > 0) {
												$("#chatContent").append(
													chatHText(
														src.val(),
														src.key
													)
												);
												$("#unsend-" + src.key).css({
													height: $(
														"#message-" + src.key
													).css("height"),
												});
												$("#unsend-" + src.key).click(
													evt => {
														$(
															"#" + evt.target.id
														).after(
															'<div class="float-right fade show alert_unsend_message alert alert-danger" role="alert" style="margin:0px 10px; width: 430px; position: relative; bottom: 20px;">' +
																"Do you want to delete this message?" +
																"<div style='display: inline-block; margin: 0px 10px; padding: 5px 20px; color: darkred; border-radius: 3px; border: 1px darkred solid;' id='yes-" +
																evt.target.id +
																"'>Yes</div>" +
																"<div style='display: inline-block; margin: 0px 5px; padding: 5px 15px;  color: green; border-radius: 3px; border: 1px green solid;' id='no-" +
																evt.target.id +
																"'>No</div>" +
																"</div>"
														);
														$(
															"#yes-" +
																evt.target.id
														).click(() => {
															console.log(
																"hello"
															);
															database
																.ref(
																	"chatroom/chat/" +
																		current_chatroom +
																		"/content/" +
																		evt.target.id.slice(
																			7
																		) +
																		"/content"
																)
																.set(
																	"Unsend a message"
																);
															$(
																"#message-" +
																	evt.target.id.slice(
																		7
																	)
															).text(
																"Unsend a message"
															);

															setTimeout(() => {
																$(
																	".alert_unsend_message"
																).fadeOut(500);
															}, 500);
														});
														$(
															"#yes-" +
																evt.target.id
														).hover(
															evt => {
																evt.target.style[
																	"color"
																] = "white";
																evt.target.style[
																	"background-color"
																] = "red";
																evt.target.style[
																	"border"
																] =
																	"1px transparent solid";
															},
															evt => {
																evt.target.style[
																	"color"
																] = "darkred";
																evt.target.style[
																	"background-color"
																] =
																	"transparent";
																evt.target.style[
																	"border"
																] =
																	"1px darkred solid";
															}
														);
														$(
															"#no-" +
																evt.target.id
														).click(() => {
															setTimeout(() => {
																$(
																	".alert_unsend_message"
																).fadeOut(500);
															}, 500);
														});
														$(
															"#no-" +
																evt.target.id
														).hover(
															evt => {
																evt.target.style[
																	"color"
																] = "white";
																evt.target.style[
																	"background-color"
																] = "green";
																evt.target.style[
																	"border"
																] =
																	"1px transparent solid";
															},
															evt => {
																evt.target.style[
																	"color"
																] = "green";
																evt.target.style[
																	"background-color"
																] =
																	"transparent";
																evt.target.style[
																	"border"
																] =
																	"1px black solid";
															}
														);

														setTimeout(() => {
															$(
																".alert_unsend_message"
															).fadeOut(5000);
														}, 3000);
													}
												);
												// ----------------------------------------------------
												let chatBox = document.getElementById(
													"chatContent_div"
												);
												let diff =
													chatBox.scrollHeight -
													chatBox.clientHeight -
													chatBox.scrollTop;
												if (diff < 700)
													scrollToBottom();

												if (src.val().uid != user.uid) {
													send_notification(
														current_chatroom,
														src.val().user,
														src.val().content
													);
												} else scrollToBottom();
											}
										}
									});
							});

						chatListF++;
						let add = new Promise((resolve, reject) => {
							$("#chatBtn").append(chatroomHText(i));
							resolve();
						});

						add.then(() => {
							$("#chat-" + i).click(() => {
								changeChatroom(i);
							});
						});
					}
				});
			}

			database.ref("chatroom/chatList").on("child_added", src => {
				let ref = database.ref("chatroom/chat/" + src.key + "/user");
				ref.once("value", data => {
					if (data.hasChild(user.uid)) {
						chatListS++;

						if (chatListS > chatListF) {
							let add = new Promise((resolve, reject) => {
								$("#chatBtn").append(chatroomHText(src.key));
								resolve();
							});

							add.then(() => {
								$("#chat-" + src.key).click(() => {
									changeChatroom(src.key);
								});
							});
						}
					}
				});
			});
		});
	});
}

function changeChatroom(src) {
	countF = 0;
	countS = 0;

	current_chatroom = src;
	$("#chat_with").text(current_chatroom);

	$("#chatContent").empty();

	// .orderByChild('time_ms')
	database.ref("chatroom/chat/" + src + "/content").once("value", data => {
		let change = false;

		for (let i in data.val()) {
			change = true;

			chatroom_count(data, src);
			countF++;

			$("#chatContent").append(chatHText(data.val()[i], i));

			$("#unsend-" + i).css({ height: $("#message-" + i).css("height") });

			$("#unsend-" + i).click(evt => {
				$("#" + evt.target.id).after(
					'<div class="float-right fade show alert_unsend_message alert alert-danger" role="alert" style="margin:0px 10px; width: 430px; position: relative; bottom: 20px;">' +
						"Do you want to delete this message?" +
						"<div style='display: inline-block; margin: 0px 10px; padding: 5px 20px; color: darkred; border-radius: 3px; border: 1px darkred solid;' id='yes-" +
						evt.target.id +
						"'>Yes</div>" +
						"<div style='display: inline-block; margin: 0px 5px; padding: 5px 15px;  color: green; border-radius: 3px; border: 1px green solid;' id='no-" +
						evt.target.id +
						"'>No</div>" +
						"</div>"
				);

				/*
                
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
						'<span aria-hidden="true">&times;</span>' +
						"</button>" +
                        */

				$("#yes-" + evt.target.id).hover(
					evt => {
						evt.target.style["color"] = "white";
						evt.target.style["background-color"] = "red";
						evt.target.style["border"] = "1px transparent solid";
					},
					evt => {
						evt.target.style["color"] = "darkred";
						evt.target.style["background-color"] = "transparent";
						evt.target.style["border"] = "1px darkred solid";
					}
				);

				$("#no-" + evt.target.id).hover(
					evt => {
						evt.target.style["color"] = "white";
						evt.target.style["background-color"] = "green";
						evt.target.style["border"] = "1px transparent solid";
					},
					evt => {
						evt.target.style["color"] = "green";
						evt.target.style["background-color"] = "transparent";
						evt.target.style["border"] = "1px black solid";
					}
				);

				setTimeout(() => {
					$(".alert_unsend_message").fadeOut(5000);
				}, 3000);

				// $("#yes-" + evt.target.id).click();
			});
		}
		scrollToBottom();

		if (!change) {
			database
				.ref("chatroom/chatList/" + src + "/num")
				.once("value", data => {
					$(".chat-num-messages").text(
						"already " + numberWithCommas(data.val()) + " messages"
					);
				});
		}
	});
}

function chatroomHText(i) {
	return (
		'<button type="button" class="w3-bar-item w3-button" id="chat-' +
		i +
		'">' +
		i +
		"</button>"
	);
}

function chatHText(data, dataKey, type) {
	// TODO: data type
	let text = data.content
		.replaceAll("&", "&amp")
		.replaceAll("<", "&lt")
		.replaceAll(">", "&gt")
		.replaceAll('"', "&quot");

	if (data.uid === user.uid) {
		return (
			'<li class="clearfix">' +
			'<div class="message-data align-right">' +
			'<span class="message-data-time">' +
			data.time +
			"</span>" +
			"&nbsp; &nbsp;" +
			'<span class="message-data-name">' +
			data.user +
			"</span>" +
			'<i class="mfa fa-circle me"></i>' +
			"</div>" +
			'<div class="message other-message float-right" id="message-' +
			dataKey +
			'">' +
			text +
			"</div>" +
			'<div class="float-right unsendBtn" id="unsend-' +
			dataKey +
			'">&times;</div>' +
			"</li>"
		);
	} else {
		return (
			"<li>" +
			'<div class="message-data"><span class="message-data-name">' +
			'<i class="fa fa-circle online"></i>' +
			data.user +
			"</span>" +
			'<span class="message-data-time">' +
			data.time +
			"</span>" +
			"</div>" +
			'<div class="message my-message">' +
			text +
			"</div>" +
			"</li>"
		);
	}
}

function getUsername(uid) {
	let ret;
	database.ref("user/" + user.uid + "/user").once("value", src => {
		ret = src.val();
	});

	return ret;
}

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function chatroom_count(data, i) {
	let chat_num = 0;

	chat_num = Object.keys(data.val()).length;

	database
		.ref("chatroom/chatList/" + i)
		.child("num")
		.set(chat_num);

	database.ref("chatroom/chatList/" + i + "/num").once("value", data => {
		$(".chat-num-messages").text(
			"already " + numberWithCommas(data.val()) + " messages"
		);
	});
}

function send_notification(title, user, content) {
	var options = {
		body: user + ": " + content,
		// ,icon: theIcon
	};

	var n = new Notification(title, options);
}

function scrollToBottom() {
	let chat_temp = document.getElementById("chatContent_div");
	$("#chatContent_div").animate(
		{ scrollTop: chat_temp.scrollHeight - chat_temp.clientHeight },
		1000
	);
}

function getRandomInt(max) {
	return Math.floor(Math.random() * max);
}
/* rule
{
  	"rules": {
		"chatroom": {
			"chat": {
				"$room": {
          "user": {
            ".read": "auth.uid != null",
						".write": "auth.uid != null"
          },
					"content": {
						".read": "data.parent().child('user').hasChild(auth.uid)",
						".write": "data.parent().child('user').hasChild(auth.uid)"
					}
				}
			},
			"chatList": {
				".read": "auth.uid != null",
				".write": "auth.uid != null"
			}
		},
		"user": {
			"$user_id": {
				".read": "auth.uid == $user_id",
				".write": "auth.uid == $user_id"
			}
		}
  	}
}
*/
